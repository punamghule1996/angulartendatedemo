import { Component, OnInit } from '@angular/core';
import { IDatePickerConfig } from 'ng2-date-picker';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
}) 
export class AppComponent implements OnInit {
  title = 'AngularTenDateDemo2';
  selectedDate: Date;
  minDate: moment.Moment;
  maxDate: moment.Moment;
  fromDate: Date;
  toDate: Date;

  config: IDatePickerConfig = {} ;
  ngOnInit()
  {
    // set min and max date return from server

  }
  
  fromDateChnange(date:moment.Moment )
  {
   if(date != undefined){
    this.config.min = date;
   }
  }
  
   toDateChange(date:moment.Moment)
   {     
     if(date != undefined){
       this.config.max = date;
      }
   }
}
